from pprint import pprint
from bs4 import BeautifulSoup
import requests

response = requests.get('https://news.ycombinator.com/news')
html = response.text

soup = BeautifulSoup(markup=html, parser='html.parser', features='lxml')

id_list = [e.get('id') for e in soup.find_all(name='tr', class_='athing')]

results = {}
for id_ in id_list:
    tr_with_id = soup.find(name='tr', class_='athing', id=id_)
    titlelink_class = tr_with_id.find(name='a', class_='titlelink')

    text = titlelink_class.getText()
    href = titlelink_class.get('href')
    try:
        score = soup.select_one(f'#score_{id_}').getText().split(' ')[0]
    except AttributeError:
        score = 0

    results[id_] = {'text': text, 'href': href, 'score': score}


pprint(results)

highest_score = -1
highest_id = -1
for key_id in results:
    result_score = int(results[key_id]['score'])
    if result_score > highest_score:
        highest_score = result_score
        highest_id = key_id

pprint(results[highest_id])

