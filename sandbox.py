from bs4 import BeautifulSoup
# import lxml

with open(file='website.html', mode='r', encoding='utf-8') as file:
    html_txt = file.read()

soup = BeautifulSoup(markup=html_txt, parser='html.parser', features='lxml')

# print(soup)
# print(soup.title)             #->   <title>Angela's Personal Site</title>
# print(soup.title.name)        #->   title
# print(soup.title.string)      #->   Angela's Personal Site
print('First anchor tag <a>:    ', soup.a)
print(' All anchor tags <a>:    ', soup.find_all(name='a'))

hyperlinks = [anchor_tag.get('href') for anchor_tag in soup.find_all(name='a')]
print(hyperlinks)

heading = soup.find(name='h1', id='name')
print(heading)

section_heading = soup.find(name='h3', class_='heading')
print(section_heading.get('class'))

company_url = soup.select_one(selector='body p em strong a')
print(company_url.get('href'))

print(soup.select(selector='.heading'))
# [<h3 class="heading">Books and Teaching</h3>, <h3 class="heading">Other Pages</h3>]
