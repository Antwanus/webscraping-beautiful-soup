from pprint import pprint
import requests
from bs4 import BeautifulSoup
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

# prompt user for date
date = input('Which year do you want to travel to? Type a date in format YYYY-MM-DD:')
# send request to billboard charts
response = requests.get(f'https://www.billboard.com/charts/hot-100/{date}')
# scrape band & title, save as list of tuples
soup = BeautifulSoup(markup=response.text, parser='html.parser', features='lxml')
band_list = [b.getText().strip('\n') for b in soup.find_all("span", class_="a-no-trucate")]
title_list = [t.getText().strip('\n') for t in soup.find_all("h3", id="title-of-a-story", class_="a-no-trucate")]

result = [(band_list[i], title_list[i]) for i in range(20)]

pprint(result)

# # # #  TEST_DATA # # #
# result = [
#     ('KC And The Sunshine Band', "Please Don't Go"),
#     ('Rupert Holmes', 'Escape (The Pina Colada Song)'),
#     ('Michael Jackson', 'Rock With You'),
# ]

# SPOTIFY
SPOTIFY_CLIENT_ID = '9f24b3123e6c45c5a70bb199c3bccb30'
SPOTIFY_CLIENT_SECRET = '9e436f96356c4cdf868aa59682027d53'
AUTH_URL = 'https://accounts.spotify.com/api/token'
BASE_URL = 'https://api.spotify.com/v1/'

auth_response = requests.post(AUTH_URL, {
    'grant_type': 'client_credentials',
    'client_id': SPOTIFY_CLIENT_ID,
    'client_secret': SPOTIFY_CLIENT_SECRET,
})

access_token = auth_response.json()['access_token']
print('access_token:', access_token)
# access_token = 'BQAMzrkv_xf-_qDZMkLQ-TvSPyAw-SjRiqIMXDgyC8C0fHe6jf8AU7r1GnYquwhnMPMkTd_pMkxGbO_nlurvQ1_xPQbooRPLD' \
#                'ney5b1o0BR-hLHR1rh5GtppBwmUv2S9fz6XpxlixgMKsay1W7VsIO2YcZMich4Xk69wdnxaGm6kE8Y56aFAhr-bRPOqyI-NcX' \
#                'YIPLUalPsnvIbS6JG3UciSw0vTQBYN'
headers = {
    'Authorization': f'Bearer {access_token}'
}

new_result = []
for tup in result[:10]:
    search_track_path = f'search?q=track:{tup[1]}%20artist:{tup[0]}&type=track'
    response = requests.get(BASE_URL + search_track_path, headers=headers)
    try:
        song_id = response.json()['tracks']['items'][0]['id']
        new_result.append({'id': song_id, 'artist': tup[0], 'track': tup[1]})
    except IndexError:
        print(f"Can't find <<<{tup[1]}>>> on Spotify, skipping...")
        continue
    except KeyError:
        print(f"Can't find <<<{tup[1]}>>> on Spotify, skipping...")
        continue

pprint(new_result)

# # # # TEST DATA # # #
# new_result = [{'artist': 'KC And The Sunshine Band',
#                'id': '6dw6BquK0Mv72p6HpusqKN',
#                'track': "Please Don't Go"},
#               {'artist': 'Rupert Holmes',
#                'id': '5IMtdHjJ1OtkxbGe4zfUxQ',
#                'track': 'Escape (The Pina Colada Song)'},
#               {'artist': 'Michael Jackson',
#                'id': '7oOOI85fVQvVnK5ynNMdW7',
#                'track': 'Rock With You'}]

res = requests.get('https://api.spotify.com/v1/me', headers=headers)
print(res.json())
user_id = '31y57pfb5bwg5flgx3op4cbyffxu' #res.json()['id']

# # # # TEST DATA # # #
# date = '1980-01-01'
# user_id = '31y57pfb5bwg5flgx3op4cbyffxu'

check_playlists_res = requests.get(f'https://api.spotify.com/v1/users/{user_id}/playlists', headers=headers)
pprint(check_playlists_res.json())

new_playlist_json = {
  "name": f"TIMEMACHINE: Back to {date}",
  "description": "This is generated via musical-timemachine/main.py",
  "public": True
}
new_playlist_response = requests.post(f'https://api.spotify.com/v1/users/{user_id}/playlists',
                                      headers=headers, json=new_playlist_json)
try:
    playlist_id = new_playlist_response.json()['id']
except KeyError:
    print('Key error...')

tracks_list = {"uris": []}
for record in new_result:
    tracks_list['uris'].append(f'spotify:track:{record["id"]}')

pprint(tracks_list)
new_playlist_response = requests.post(f'https://api.spotify.com/v1/playlists/{playlist_id}/tracks',
                                      headers=headers, json=tracks_list, )

print(new_playlist_response.json())

