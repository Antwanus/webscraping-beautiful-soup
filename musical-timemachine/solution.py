from bs4 import BeautifulSoup
import requests
import spotipy
from spotipy.oauth2 import SpotifyOAuth

SPOTIFY_CLIENT_ID = '9f24b3123e6c45c5a70bb199c3bccb30'
SPOTIFY_CLIENT_SECRET = '9e436f96356c4cdf868aa59682027d53'

date = input("Which year do you want to travel to? Type the date in this format YYYY-MM-DD: ")

# Scraping Billboard 100
response = requests.get(f'https://www.billboard.com/charts/hot-100/{date}')
# scrape band & title, save as list of tuples
soup = BeautifulSoup(markup=response.text, features='html.parser')
band_list = [b.getText().strip() for b in soup.find_all("span", class_="a-no-trucate")]
title_list = [t.getText().strip() for t in soup.find_all("h3", id="title-of-a-story", class_="a-no-trucate")]
print(title_list)


# authentication/get client
sp = spotipy.Spotify(
    auth_manager=SpotifyOAuth(
        scope="playlist-modify-private",
        redirect_uri="http://example.com",
        client_id=SPOTIFY_CLIENT_ID,
        client_secret=SPOTIFY_CLIENT_SECRET,
        show_dialog=True,
        cache_path="./token.txt"
    )
)
# search spotify for title & band   +   create list of uris
song_uris = []
year = date.split("-")[0]
for i in range(len(title_list)):
    song = title_list[i]
    artist = band_list[i]
    print(song)
    result = sp.search(q=f"artist:{artist} track:{song} year:{year}", type="track")
    print(result)
    try:
        uri = result["tracks"]["items"][0]["uri"]
        song_uris.append(uri)
    except IndexError:
        print(f"{song} doesn't exist in Spotify. Skipped.")
# create playlist
playlist = sp.user_playlist_create(user=sp.current_user()["id"], name=f"{date} Billboard 100", public=False)
print(playlist)
# add songs to playlist
print('Created a new playlist! id=\'\'', playlist['id'])
sp.playlist_add_items(playlist_id=playlist["id"], items=song_uris)
