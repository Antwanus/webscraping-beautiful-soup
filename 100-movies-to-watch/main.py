from pprint import pprint
import requests
from bs4 import BeautifulSoup

html = requests.get('https://web.archive.org/web/20200518073855/https://www.empireonline.com/movies/features/best'
                    '-movies-2/').text

soup = BeautifulSoup(markup=html, parser='html.parser', features="lxml")

all_movie_titles = [e.getText() for e in soup.find_all(name='h3', class_='title')]

# pprint(all_movie_titles)
# results.sort(reverse=True)
all_movie_titles = all_movie_titles[::-1]
pprint(all_movie_titles)

with open('top_100_movies.txt', 'w', encoding='utf-8') as outfile:
    for title in all_movie_titles:
        outfile.write(f'{title}\n')
